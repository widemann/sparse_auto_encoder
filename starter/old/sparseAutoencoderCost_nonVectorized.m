function [cost,grad] = sparseAutoencoderCost_nonVectorized(theta, visibleSize, hiddenSize, ...
                                             lambda, sparsityParam, beta, data)

% visibleSize: the number of input units (probably 64) 
% hiddenSize: the number of hidden units (probably 25) 
% lambda: weight decay parameter
% sparsityParam: The desired average activation for the hidden units (denoted in the lecture
%                           notes by the greek alphabet rho, which looks like a lower-case "p").
% beta: weight of sparsity penalty term
% data: Our 64x10000 matrix containing the training data.  So, data(:,i) is the i-th training example. 
  
% The input theta is a vector (because minFunc expects the parameters to be a vector). 
% We first convert theta to the (W1, W2, b1, b2) matrix/vector format, so that this 
% follows the notation convention of the lecture notes. 

W1 = reshape(theta(1:hiddenSize*visibleSize), hiddenSize, visibleSize);
W2 = reshape(theta(hiddenSize*visibleSize+1:2*hiddenSize*visibleSize), visibleSize, hiddenSize);
b1 = theta(2*hiddenSize*visibleSize+1:2*hiddenSize*visibleSize+hiddenSize);
b2 = theta(2*hiddenSize*visibleSize+hiddenSize+1:end);

% Cost and gradient variables (your code needs to compute these values). 
% Here, we initialize them to zeros. 
cost = 0;
W1grad = zeros(size(W1)); 
W2grad = zeros(size(W2));
b1grad = zeros(size(b1)); 
b2grad = zeros(size(b2));

%% ---------- YOUR CODE HERE --------------------------------------
%  Instructions: Compute the cost/optimization objective J_sparse(W,b) for the Sparse Autoencoder,
%                and the corresponding gradients W1grad, W2grad, b1grad, b2grad.
%
% W1grad, W2grad, b1grad and b2grad should be computed using backpropagation.
% Note that W1grad has the same dimensions as W1, b1grad has the same dimensions
% as b1, etc.  Your code should set W1grad to be the partial derivative of J_sparse(W,b) with
% respect to W1.  I.e., W1grad(i,j) should be the partial derivative of J_sparse(W,b) 
% with respect to the input parameter W1(i,j).  Thus, W1grad should be equal to the term 
% [(1/m) \Delta W^{(1)} + \lambda W^{(1)}] in the last block of pseudo-code in Section 2.2 
% of the lecture notes (and similarly for W2grad, b1grad, b2grad).
% 
% Stated differently, if we were using batch gradient descent to optimize the parameters,
% the gradient descent update to W1 would be W1 := W1 - alpha * W1grad, and similarly for W2, b1, b2. 
% 
m = size(data,2);

% cost and gradient computation.  
if beta~=0 % functional has a sparsity term. 
    % forward pass saving results for sparsity estimation.
    a_2 = zeros(hiddenSize,m);
    d_3 = zeros(visibleSize,m);
    for k = 1:m
        z_2 = W1*data(:,k)+b1; 
        a_2(:,k) = sigmoid(z_2);
        z_3 = W2*a_2(:,k) + b2;
        h = sigmoid(z_3);
        d_3(:,k) = (h - data(:,k)).*(h.*(1-h));
        cost = cost + norm(h-data(:,k))^2;
    end
    cost = (1/(2*m))*cost + (lambda/2)*(sum(W1(:).^2) + sum(W2(:).^2));
    
    % add sparsity to the cost.
    rho_hat = mean(a_2,2);
    rho = sparsityParam;
    sparsity_penalty = rho*log(rho./rho_hat) + (1-rho)*log((1-rho)./(1-rho_hat));
    cost = cost + beta*sum(sparsity_penalty(:));
    
    % compute the derivative.
    for k = 1:m
        d_2 = (W2'*d_3(:,k) + beta*(-rho./rho_hat + (1-rho)./(1-rho_hat))).*(a_2(:,k).*(1-a_2(:,k)));

        DJ_W1 = d_2*data(:,k)';
        DJ_W2 = d_3(:,k)*a_2(:,k)';
        DJ_b1 = d_2;
        DJ_b2 = d_3(:,k);

        W1grad = W1grad + DJ_W1;
        W2grad = W2grad + DJ_W2;
        b1grad = b1grad + DJ_b1;
        b2grad = b2grad + DJ_b2;
    end
    W1grad = (1/m)*W1grad + lambda*W1;
    W2grad = (1/m)*W2grad + lambda*W2;
    b1grad = (1/m)*b1grad;
    b2grad = (1/m)*b2grad;
else
    for k = 1:m
        z_2 = W1*data(:,k)+b1; 
        a_2 = sigmoid(z_2);
        z_3 = W2*a_2 + b2;
        h = sigmoid(z_3);
        cost = cost + norm(h-data(:,k))^2;
        
        d_3 = (h - data(:,k)).*(h.*(1-h)); 
        d_2 = (W2'*d_3).*(a_2.*(1-a_2));
        %d_1 = (W1'*d_2).*(data(:,k).*(1-data(:,k)));


        DJ_W1 = d_2*data(:,k)';
        DJ_W2 = d_3*a_2';
        DJ_b1 = d_2;
        DJ_b2 = d_3;

        W1grad = W1grad + DJ_W1;
        W2grad = W2grad + DJ_W2;
        b1grad = b1grad + DJ_b1;
        b2grad = b2grad + DJ_b2;
    end
    cost = (1/(2*m))*cost + (lambda/2)*(sum(W1(:).^2) + sum(W2(:).^2));
    W1grad = (1/m)*W1grad + lambda*W1;
    W2grad = (1/m)*W2grad + lambda*W2;
    b1grad = (1/m)*b1grad;
    b2grad = (1/m)*b2grad;
end

% W1 = W1 - alpha*(m1*W1grad + lambda*W1);
% W2 = W2 - alpha*(m1*W2grad + lambda*W2);
% b1 = b1 - alpha*(m1*b1grad);
% b2 = b2 - alpha*(m1*b2grad);

%-------------------------------------------------------------------
% After computing the cost and gradient, we will convert the gradients back
% to a vector format (suitable for minFunc).  Specifically, we will unroll
% your gradient matrices into a vector.

grad = [W1grad(:) ; W2grad(:) ; b1grad(:) ; b2grad(:)];

end



















