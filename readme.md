This is matlab code for creating a deep neural network. The final product is in the folder 
**stackedae_exercise**. (stacked auto-encoder) 

The auto-encoders used to train the first two levels of the deep neural network. 

The code walks one through an example of building a classifier for hand-written digits. 

Folders:

mnistHelper: Data and how to read it in.

starter: How to create a sparse auto-encoder

stl_exercise: Feed forward auto-encoder

vectorization: Vectorizing auto-encoder code. 

pca_exercise: Use PCA to compress each image.

pca_2d: PCA for data

softmax: Turn the output into a probability.

stackedae_exercise: Complete deep neural network.