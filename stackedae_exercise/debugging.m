
%% DEBUGGING!!!
a = genpath('..');
addpath(a);
%%======================================================================
%% STEP 0: Here we provide the relevant parameters values that will
%  allow your sparse autoencoder to get good filters; you do not need to 
%  change the parameters below.
M = 9;
inputSize = 3*3;
numClasses = 10;
hiddenSizeL1 = 2;    % Layer 1 Hidden Size
hiddenSizeL2 = 2;    % Layer 2 Hidden Size
sparsityParam = 0.1;   % desired average activation of the hidden units.
                       % (This was denoted by the Greek alphabet rho, which looks like a lower-case "p",
		               %  in the lecture notes). 
lambda = 3e-3;         % weight decay parameter       
beta = 3;              % weight of sparsity penalty term       

%%======================================================================
%% STEP 1: Load data from the MNIST database
%
%  This loads our training data from the MNIST database files.

% Load MNIST database files
trainData = randn(inputSize,M);
trainLabels = randi(numClasses,1,M);
trainLabels(trainLabels == 0) = 10; % Remap 0 to 10 since our labels need to start from 1

sae1Theta = initializeParameters(hiddenSizeL1, inputSize);

sae2Theta = initializeParameters(hiddenSizeL2, hiddenSizeL1);

saeSoftmaxTheta = 0.005 * randn(hiddenSizeL2 * numClasses, 1);

stack = cell(2,1);
stack{1}.w = reshape(sae1Theta(1:hiddenSizeL1*inputSize), ...
                     hiddenSizeL1, inputSize);
stack{1}.b = sae1Theta(2*hiddenSizeL1*inputSize+1:2*hiddenSizeL1*inputSize+hiddenSizeL1);
stack{2}.w = reshape(sae2Theta(1:hiddenSizeL2*hiddenSizeL1), ...
                     hiddenSizeL2, hiddenSizeL1);
stack{2}.b = sae2Theta(2*hiddenSizeL2*hiddenSizeL1+1:2*hiddenSizeL2*hiddenSizeL1+hiddenSizeL2);

% Initialize the parameters for the deep model
[stackparams, netconfig] = stack2params(stack);
stackedAETheta = [ saeSoftmaxTheta ; stackparams ];

numGrad = computeNumericalGradient( @(x) stackedAECost(x, inputSize, hiddenSizeL2,...
                                        numClasses, netconfig,...
                                        lambda, trainData, trainLabels),...
                                        stackedAETheta);

[ cost, grad ] = stackedAECost(stackedAETheta, inputSize, hiddenSizeL2, ...
                    numClasses, netconfig, ...
                    lambda, trainData, trainLabels);

% Use this to visually compare the gradients side by side
disp([numGrad grad]); 

% Compare numerically computed gradients with those computed analytically
diff = norm(numGrad-grad)/norm(numGrad+grad);
disp(diff); 
